---
title: be your potential
---

# LES FORMATIONS PAR BENEXT

Le monde dans lequel nous vivons est devenu complexe, incertain et ambigu. Les entreprises rencontrent des situations à la fois mouvantes et parfois même inattendues. Si cela n’était pas suffisant, ajoutons que ce monde change vite, de plus en plus vite. Pour ces entreprises, le salut vient dans leur faculté à s’adapter pour survivre, à innover pour se démarquer mais aussi à prendre les meilleures décisions au moment où cela est nécessaire. Alors comment s’y prendre ? quels sont les outils? quelle est LA méthode? Nous pensons qu’ il n’existe pas UNE méthode , immuable qui s’applique de la même façon partout : ni entre les entreprises ni même au sein des différents services d’une entreprise. Il est évidemment pertinent de s’inspirer d’un modèle, mais pas de le répliquer, gare au cargo cult. Chaque culture est différente, chaque métier est différent, chaque équipe est différente. Pour autant, comme toute discipline (artistique, sportive, …), il y a un certain nombre de fondamentaux à maîtriser et des réflexes à acquérir.

C’est ce que propose nos formations : des fondamentaux à maîtriser et des réflexes à acquérir à travers nos ateliers qui favorisent l’acquisition des connaissances par la répétition du geste et l’invitation à une riche conversation entre pairs.