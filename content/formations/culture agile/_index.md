---
title: Culture agile
color: "#EEF1EA"
code: "3.21.12.20.21.18.5 1.7.9.12.5"
weight: 10
image : "culture-agile"
---


L’enjeu de ce cursus, constitué de quatre formations autonomes, est multiple et généraliste. Il aborde les sujets suivants :

* Qu’est-ce que l'agilité ? Pourquoi l’agilité ?
* Impacts et bénéfices de cette approche, ses limites
* Deux mises en œuvre connues et reconnues de l’approche agile : le framework scrum, le framework kanban.
* L’agilité à l’échelle : implémentation et bénéfices de l’agilité lorsque plusieurs équipes travaillent sur le même produit.

Vous pouvez suivre les quatre formations ou sélectionner celle qui vous paraît la plus adaptée à vos besoins.

Mentor du domaine
- Loïc Le Molgat
