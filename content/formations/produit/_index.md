---
title: Le produit
color: "#FBFDBC"
code: "16.18.15.4.21.9.20"
weight: 20
image : "produit"
---

Le produit est le cœur de l’entreprise, le moyen, le levier pour exister et croître. Il doit être utile, adapté, performant et attachant. Et combler l’utilisateur dans son usage.

Ce cursus, composé de six formations, est exclusivement basé sur le développement du produit et des personnes qui le font vivre.
* Découverte et approfondissement du product ownership et du product backlog
* Le design sprint
* Le product management sur tout son cycle de vie, gestion d’équipes produit
* La stratégie produit, identification et déclinaison des OKR
* Maîtriser la transition du besoin métier à la solution technique et éviter la dette fonctionnelle

Vous pouvez suivre l’ensemble de ces formations ou sélectionner celle qui vous paraît la plus adaptée à vos besoins. Certaines demandent en pré-requis des compétences en agilité ou gestion de produit.

Mentores du domaine
- Tiphanie Vinet
- Laurence Wolff 
