---
title: Développement IT
section: developpement IT
color: "#A4DCF7"
code: "4.5.22.5.12.15.16.16.5.13.5.14.20 9.20"
weight: 50
image : "developpement-it"
---



La qualité et la compréhension du besoin sont des fondamentaux du développement logiciel qui sont indispensables pour livrer régulièrement de la valeur business en production de façon soutenable et sans stress.
Le cursus technique actuel est axé autour de ces 2 fondamentaux.

* Améliorer la qualité du code par le test : Apprendre les bases des tests automatisés et du design guidé par les tests
* Du métier au code: Découvrir les outils permettant de mieux comprendre le besoin et mieux le projeter dans le code tout en le documentant.

En tant que développeur, les 2 formations peuvent être suivies de façon indépendante. Cependant la seconde demande plus de maturité et avoir quelques années d'expérience dans le développement d'applications métier.

Mentor du domaine
- Yannick Grenzinger




