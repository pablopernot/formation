---
title: Leadership
color: "#ACD8C9"
code: "leadership"
weight: 30
image : "leadership"
---


Ce cursus propose des formations sur les compétences douces, les compétences comportementales liées à la posture, le savoir-être, complément indispensable aux compétences techniques et métier.

* Apprendre à dire les choses
* Apprendre à pitcher
* Management agile 1 : Du manager au leader
* Management agile 2 : Piloter dans un monde complexe

Mentors du domaine
- Pauline Garric
- Pablo Pernot




