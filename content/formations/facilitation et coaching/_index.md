---
title: Facilitation et coaching
color: "#F5D9D8"
code: "6.1.3.9.12.9.20.1.20.9.15.14 5.20 3.15.1.3.8.9.14.7"
weight: 40 
image : "facilitation-et-coaching"
---


La facilitation et le coaching font désormais partie intégrante des organisations, ce sont des leviers utiles qui, une fois activés, aident efficacement les équipes et les individus à mieux communiquer, à mieux collaborer, à mieux prioriser, à prendre de meilleures décisions, à concevoir de meilleurs produits.

Ce cursus, composé de 5 formations graduelles, est exclusivement basé sur le développement de la posture de facilitateur et de coach.

* Facilitation 1 : La posture de facilitateur et les fondamentaux de l'animation du collectif
* Coaching d’équipe 1 : Découverte du rôle de Scrum Master et de l'accompagnement d'une équipe agile
* Coaching d’équipe 2 : Du perfectionnement de sa posture aux outils pour aller accompagner plus loin
* Coaching d’organisation 1 : De l’équipe à l’organisation, accompagner plusieurs équipes
* Coaching organisation 2 : Devenir une entreprise agile dans un monde complexe


Vous pouvez suivre les trois formations ou sélectionner celle qui vous paraît la plus adaptée à vos besoins. Certaines demandent en pré-requis des compétences en agilité ou gestion de produit.


Mentors du domaine
- Pablo Pernot
- Dragos Dreptate




