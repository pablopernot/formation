---
title: Notre approche
slug: /notre-approche/
---

Comme toute discipline artistique, sportive, il y a un certain nombre de fondamentaux à maîtriser et des réflexes à acquérir.

C’est ce que proposent nos formations : un peu de théorie pour maîtriser les fondamentaux et beaucoup d’ateliers pratiques pour acquérir ces nouveaux réflexes par la répétition du geste. 

Notre approche est pédagogique et participative : l’essentiel du temps passé consiste à simuler des situations courantes dans la vie des projets et faire des exercices de groupe.

Notre méthodologie consiste à construire du savoir par des débriefings, autour de conversations (simulations, mises en pratique, brainstorming, exemples de cas pratiques de la vie professionnelle, introspection, codéveloppement, questions / réponses). 
