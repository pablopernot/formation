---
title: A propos de benext
image: /images/Olgaç Bozalp.jpg
---

Pour notre vision de l'apprentissage. En favorisant des expériences collaboratives et participatives, nous cherchons à générer de nouveaux regards, de nouvelles émotions, de nouvelles perspectives, pour déclencher, statuer, découvrir, décider, là où dans le quotidien c’est impossible.

Pour nos coachs-formateurs de terrain expérimentés et reconnus par leurs pairs. En 3 ans, ils ont proposé 50 formations et permis à 350 personnes de vivre l'experience BENEXT. Grandir et faire grandir.

Pour notre soutien pour intégrer les compétences et les connaissances nécessaires à la réalisation de vos produits et à la réussite de vos équipes. Suite à la formation, ce n'est pas fini, nous vous proposons une séance de coaching d'une heure pour mettre le pied à l'étrier et nous vous aidons en vous proposant ,toute une année, des contenus de qualité relatifs à vos besoins.

Pour notre expertise en organisation d’événements pour animer des collectifs. C'est plus de 100 événements: conférences / journées / séminaires en france et à l’étranger (Barcelone, Maroc, Kiev, Bruxelles) jusqu’à 350 personnes pour nos clients et pour nous 

Nous avons créé des conférences et notre équipe de coachs / facilitateur /game designer intervient régulièrement dans l’organisation et la facilitation de conférences reconnues comme School Of Product, BECOM X, BECOM, FlowCon, Orange Agile Day, BNP Agile day...

Pour nos locaux de 1500 m2 originaux et fun en plein coeur de Paris (et nos outils pour tout faire à distance !) 

