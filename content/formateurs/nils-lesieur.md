---
title: Nils Lesieur
photo: nils.jpeg
code: 14.9.12.19 12.5.19.9.5.21.18
---


Coaching Produit. Coachng agile. Nils aide les équipes et les organisations à devenir conscientes. Conscientes des buts à atteindre, de leurs capacités, de leur quotidien et de leur environnement.
