---
title: Laurence Wolff
photo: laurence.jpeg
code: 12.1.21.18.5.14.3.5 23.15.12.6.6.
---


Coaching Produit. Laurence apporte son expertise prosuit et son savoir faire pour permettre aux personnes de s’exprimer et mieux s’épanouir. Son animation permet également de se remettre en cause et de s’améliorer en permanence au contact des autres.
