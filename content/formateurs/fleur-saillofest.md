---
title: Fleur Saillofest
photo: fleur.jpeg
code: 6.12.5.21.18 19.1.9.12.12.15.6.5.19.20
---


Coaching agile.
Convaincue que les équipes peuvent évoluer et s'améliorer dans leur manière de travailler, Fleur souhaite aider les personnes, les équipes, les organisations à mieux communiquer et à mieux s’organiser.
