---
title: Yannick Grezinger
photo: yannick.jpeg
code: 25.1.14.14.9.3.11 7.18.5.26.9.14.7.5.18
---


Coaching tech. Yannick aide les personnes qui font du logiciel à être fières de ce qu’elles font et qu’elles puissent grandir dans leur vie professionnelle et personnelle. Il aide au quotidien les entreprises à dépasser les défis du 21ème siècle et la complexité du développement logiciel.

