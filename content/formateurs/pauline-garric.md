---
title: Pauline Garric
photo: pauline.jpeg
code: 16.1.21.12.9.14.5 7.1.18.18.9.3
---


Coaching d'équipe. Coach executive certifié HEC, Pauline aborde les problématiques organisationnelles en s'appuyant sur le mindset agile et les softskills. Process Com, CNV, gestion de conflit, feedback, intelligence socio-émotionnelle... sont pour elle des clés pour améliorer la performance d'équipe et sa communication.
