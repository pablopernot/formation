---
title: Loïc Le Molgat
photo: loic.jpg
code: 12.15.9.3 12.5 13.15.12.7.1.20
---


Coaching d'organisation. Management agile. Leadership. Loïc aide les équipes et les organisations à répondre aux nouveaux enjeux d'un monde qui change en favorisant l’émergence de pratiques efficaces et adaptées à leur culture.
