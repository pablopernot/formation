---
title: Jonathan Litty
photo: jo.jpeg
code: 10.15.14.1.20.8.1.14 12.9.20.20.25
---


Coaching produit. Jonathan aide les équipes à adopter le mindset Produit, à se poser les bonnes questions, à trouver le juste milieu entre la réalité marché et la stratégie business, et à produire les expériences utilisateur à forte valeur.
