---
title: Tiphanie Vinet 
photo: tiphanie.jpeg
code: 20.9.16.8.1.14.9.5 22.9.14.5.20
---


Coaching produit. Tiphanie met à profit son expertise du management produit pour créer ensemble des choses qui ont du sens et un véritable impact pour leur business.
