# The Hackademy


## Le guide pour écrire en markdown 

[Guide Markdown](https://www.markdownguide.org/cheat-sheet)

## Saisir une formation 

1. Saisir le fichier formation dans le dossier data/catalogue
2. Placer un fichier md (prendre un fichier existant) et bien mettre en clef "datafile" le nom du fichier data sans l'extension .toml. 
3. Si vous voulez un url d'inscription spécifique vous ajoutez 
	specific="https://www.eventbrite.com/e/in-person-passionate-product-leadership-workshop-paris-france-tickets-158493214471"
dans le fichier data de la formation

**LES DATES SONT TOUJOURS AU FORMAT : "2021-11-23" YYYY-MM-DD.** 

## Les images

* Les images sont dans /static/images 
* Pour les formations il va chercher directement dans /static/images/formations donc vous indiquez simplement le nom de l'image
* Pour les formateurs il va chercher directement dans /static/images/formateurs donc vous indiquez simplement le nom de l'image

## Le catalogue PDF

* Doit s'appeler benext-hackademy-catalogue-de-formations.pdf et être placé dans /static/pdf/

## Les fiches PDF 

* Doivent se trouver dans /pdf/fiches/ **ET PORTER LE MÊME NOM QUE LE FICHIER DATA** et **OUI** la casse compte. 
* Si vous ne voulez pas montrer de fiche PDF dans la fiche data de la formation vous ajoutez : 
	showpdf=false
	
### Suivi 

#### Point du 2 août 2021

* présents : Jonathan, Alexandre P, Thomas A, Pablo 

* Lien du figma : https://www.figma.com/file/50AIxDnlA5YHSA1dc8hKa7/Untitled?node-id=0%3A1
* Essayer de mettre les "strong" aux couleurs du domaine
* penser au 404, + merci + saisie pré-inscription 
* tagmanager
* search console
* redirections 
* Page contact (thomas)  -> retombe sur merci 

